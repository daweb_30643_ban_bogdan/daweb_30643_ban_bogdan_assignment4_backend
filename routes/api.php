<?php
use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register',[AuthController::class,'register']);
Route::post('/login',[AuthController::class,'login']);
// Route::resource('apps',AppointmentController::class);
Route::get('/apps',[AppointmentController::class,'index']);
Route::put('/apps/{id}',[AppointmentController::class,'update']);
Route::get('apps/search/{name}',[AppointmentController::class,'search']);
Route::get('/userr/{id}',[AuthController::class,'show']);
Route::get('/userr',[AuthController::class,'index']);

Route::group(['middleware'=>['auth:sanctum']], function () {
    // Route::get('apps/search/{name}',[AppointmentController::class,'search']);
    Route::post('/logout',[AuthController::class,'logout']);
    Route::post('/apps',[AppointmentController::class,'store']);
    Route::put('/userr/{id}',[AuthController::class,'update']);
});

// Route::get('apps/', [AppointmentController::class,'index']);
// Route::post('apps/', [AppointmentController::class,'store']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Route::resource('gcalendar', 'gCalendarController');
// Route::get('oauth', ['as' => 'oauthCallback', 'uses' => 'gCalendarController@oauth']);
// Route::resource('cal', 'gCalendarController');
// Route::get('oauth', 'gCalendarController@oauth')->name('oauthCallback');